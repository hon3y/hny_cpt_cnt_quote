<?php
namespace HIVE\HiveCptCntQuote\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_quote" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Quotes
 */
class QuoteRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveCptCntQuote\\Domain\\Model\\Quote';
        $sUserFuncPlugin = 'tx_hivecptcntquote';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string String containing uids
     * @return \HIVE\HiveCptCntQuote\Domain\Model\Quote
     */
    public function findByUidListOrderByListIfNotEmpty($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }

        //return only if not empty
        if ($result[0] != NULL){
            return $result;
        }
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return Quote
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

//    /**
//     *
//     * Returns all objects of this repository with matching category
//     *
//     * @param \HIVE\HiveCptCntQuote\Domain\Model\Quote $category
//     *
//     * @return \HIVE\HiveCptCntQuote\Domain\Model\Quote
//     */
//    public function findByCategory(Tx_HiveCptCntQuote_Domain_Model_Quote $category) {
//        $query = $this->createQuery();
//        $query->matching($query->contains('categories', $category));
//        $result = $query->execute();
//        return $result;
//    }
}