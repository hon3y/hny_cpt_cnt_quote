<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntQuote',
            'Hivequotelistquote',
            'hive :: Quote :: listQuote'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntQuote',
            'Hivequoteshowquote',
            'hive :: Quote :: showQuote'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_quote', 'Configuration/TypoScript', 'hive_cpt_cnt_quote');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntquote_domain_model_quote', 'EXT:hive_cpt_cnt_quote/Resources/Private/Language/locallang_csh_tx_hivecptcntquote_domain_model_quote.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntquote_domain_model_quote');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_cpt_cnt_quote',
            'tx_hivecptcntquote_domain_model_quote'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('hivequotelistquote');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');
