
plugin.tx_hivecptcntquote_hivequotelistquote {
    view {
        # cat=plugin.tx_hivecptcntquote_hivequotelistquote/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntquote_hivequotelistquote/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntquote_hivequotelistquote/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntquote_hivequotelistquote//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntquote_hivequoteshowquote {
    view {
        # cat=plugin.tx_hivecptcntquote_hivequoteshowquote/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntquote_hivequoteshowquote/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntquote_hivequoteshowquote/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_quote/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntquote_hivequoteshowquote//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntquote {

        persistence {
            storagePid =
        }

        model {
            HIVE\HiveCptCntQuote\Domain\Model\Quote {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

plugin.tx_hivecptcntquote_hivequotelistquote.settings.production.list.pid = 123
plugin.tx_hivecptcntquote_hivequotelistquote.settings.production.list.itemsPerPage = 4
plugin.tx_hivecptcntquote_hivequotelistquote.settings.production.list.maximumNumberOfLinks = 3

plugin.tx_hivecptcntquote_hivequoteshowquote.persistence.storagePid = 123