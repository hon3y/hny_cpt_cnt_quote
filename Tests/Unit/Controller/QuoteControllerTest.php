<?php
namespace HIVE\HiveCptCntQuote\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class QuoteControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntQuote\Controller\QuoteController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HIVE\HiveCptCntQuote\Controller\QuoteController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllQuotesFromRepositoryAndAssignsThemToView()
    {

        $allQuotes = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $quoteRepository = $this->getMockBuilder(\HIVE\HiveCptCntQuote\Domain\Repository\QuoteRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $quoteRepository->expects(self::once())->method('findAll')->will(self::returnValue($allQuotes));
        $this->inject($this->subject, 'quoteRepository', $quoteRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('quotes', $allQuotes);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenQuoteToView()
    {
        $quote = new \HIVE\HiveCptCntQuote\Domain\Model\Quote();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('quote', $quote);

        $this->subject->showAction($quote);
    }
}
